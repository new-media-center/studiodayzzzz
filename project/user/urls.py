from django.urls import path
from .views import usrView, usrDel

urlpatterns = [ path ("", usrView, name="user-list")
              , path ("<int:pk>/", usrView, name="user-update")
              , path ("<int:pk>/del/", usrDel, name="user-del")
              ]
