from mozilla_django_oidc.auth import OIDCAuthenticationBackend

class MyOIDCAB(OIDCAuthenticationBackend):
    def verify_claims(self, claims):

        verified = super(MyOIDCAB, self).verify_claims(claims)

        return verified and claims["email_verified"]

    def create_user(self, claims):

        verified = super(MyOIDCAB, self).verify_claims(claims)

        if verified and claims["email_verified"]:
            user = super(MyOIDCAB, self).create_user(claims)
            user.first_name = claims.get('given_name', '')
            user.last_name = claims.get('family_name', '')
            user.email = claims.get('email', '')
            user.save()
            return user
        return None

    def update_user(self, user, claims):

        verified = super(MyOIDCAB, self).verify_claims(claims)

        if verified and claims["email_verified"]:
            user.first_name = claims.get('given_name', '')
            user.last_name = claims.get('family_name', '')
            user.email = claims.get('email', '')
            user.save()
            return user

        return None
