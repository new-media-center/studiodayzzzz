# Wie läuft eine Studio Days-Session ab?

1. Sie buchen über unser [Webtool](/appointment/free-slots) einen Termin.
2. Jeder Termin hat eine Maximallänge von 30 Minuten.
3. Sie kommen in das Studio, wir erklären kurz das Szenario, und dann beginnen
   wir mit der Aufnahme.
4. Einen kleinen Wunsch hätten wir: **seien Sie 10 Minuten vorher da**, damit
   wir genügend Zeit für Sie haben und alles reibungslos verläuft.
5. Bitte beachten Sie, dass Ihre eingereichten Slides idealerweise die
   endgültige Version sein sollten. Während der Aufnahme haben wir nur begrenzt
   die Gelegenheit, die Slides zu ändern.
6. Gegen Ende der 30 Minuten signalisieren wir, dass die Zeit abgelaufen ist.
7. Das fertige Video liefern wir **innerhalb von ein bis zwei Wochen** ab.

# Wie lange wird mein Video sein?

Die fertigen Videos sind **höchstens 5 bis 10 Minuten** lang. Die 30 Minuten
Drehzeit im Studio entsprechen also nicht einer 30-minütigen Präsentation.  Wir
empfehlen, kurze und knackige Präsentationen mit ein paar wenigen Slides zu
planen – keine ganzen Vorlesungen.

# Wie soll ich meine Slides einschicken?

Schicken Sie Ihre Slides als **PDF und PowerPoint** (kein Pages oder Keynote)
**spätestens 2 Tage vor Ihrem Termin** an unsere Email-Adresse:
**contact-nmc@unibas.ch.**

Falls Filme oder Animationen in die Präsentation eingebettet sind, diese
bitte separat mitschicken. Bitte verzichten Sie auf Übergänge zwischen den
Slides, da wir diese im Schnitt nicht einbeziehen können.

[Hier ist eine PowerPoint
Vorlage](/static/downloadables/StudioDays_PowerPoint_Template_DE.pptx), die Sie
gerne benutzen können.  Im Studio sehen Sie nur die Slides – keinen
Presenter-Modus. Wir empfehlen deshalb, Ihren
Text vorher zu üben.

**Tipp:** Gut lesbare Slides sind unerlässlich. Der Text sollte nicht zu klein
sein und die Slides nicht zu vollgepackt.

# Wie bereite ich mich vor?

Für einen gelungenen Auftritt vor der Kamera üben Sie Ihren Text am besten im
Voraus, um souverän frei sprechen zu können. Das schenkt Ihnen nicht nur
Selbstvertrauen, sondern auch ein Gespür für die Dauer des Videos. Weitere
Tipps zum Schreiben Ihres Textes und Ihrer Slides finden Sie unter [Writing for
Multimedia](https://tales.nmc.unibas.ch/de/writing-for-multimedia-45/){: target="_blank"}.
