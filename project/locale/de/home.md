Sie wünschen sich ein professionell produziertes Präsentationsvideo?  
Dann melden Sie sich bei unseren Studio Days an!
{ .teaser }

Als **Angehörige der Universität Basel** können Sie an den Studio Days** ein
Zeitfenster von 30 Minuten** im Studio des New Media Centers buchen und
zusammen mit uns ein kurzes, einfaches Video produzieren. Sie bringen die
Inhalte und Performance mit; wir liefern die Technik und den Schnitt!

![Eine Stopuhr](/static/images/01_aufnahmezeit.svg "30 Minuten Aufnahmezeit")
{ .illustrated-offer }

![Generische Darstellung eines Videos](/static/images/02_video.svg "5- bis 10-minütiges fertiges Video")
{ .illustrated-offer }

![Generische Darstellung zweier Slides](/static/images/03_slides.svg "Ihre eigenen Slides im Video")
{ .illustrated-offer }

![Eine Filmklappe](/static/images/04_professionell.svg "Professionelle Kamera, professioneller Ton und Schnitt")
{ .illustrated-offer }

# Wie funktioniert es?

Sie stehen vor einem Farbhintergrund in einem professionell ausgeleuchteten Set und sehen während der Aufnahme Ihre vorbereiteten Slides. Nach dem Dreh schneiden wir die Aufnahme und Ihre Slides zusammen und schicken Ihnen das fertige Video als MP4-Datei. Buchen Sie [hier](/appointment/free-slots) einen Termin!
