# What kind of content can I film?

Studio Days sessions are intended for university topics within the fields of science, teaching and research. Disclosure is required for commercial or private sector funding.

# What about image and graphics copyright?

The Studio Days service does **not** include a check of your content for copyright infringement. Please clarify the rights to the images and graphics in your slides in advance. Only use images and graphics to which you have the rights. The safest way is to **take the pictures or create the graphics yourself** or to **use royalty-free images,** for example from Pixabay, Unsplash, Pexels, and so on. More information on image rights at the University of Basel can be found under [Bildrechte & Bildunterschriften (German)](https://www.unibas.ch/de/Universitaet/Administration-Services/Bereich-Rektorin/Kommunikation-Marketing/Web-Services/Web-Desk/Bildrechte-Bildunterschriften.html){: target='_blank'}, [Online Image Rights & Photo Credits (English)](https://www.unibas.ch/en/University/Administration-Services/The-President-s-Office/Communications-Marketing/Web-Services/Web-Desk/Online-Image-Rights_Photo-Credits.html){: target='_blank'}, [Intellectual Property Rights](https://researchdata.unibas.ch/en/legal-issues/intellectual-property-rights/){: target='_blank'} and in the PDF [Datenschutz & Urheberrecht](https://www.unibas.ch/dam/jcr:4d0daf2a-1980-4e36-a939-64f8da58cc0d/Merkblatt_Datenschutz_Urheberrecht_20200313.pdf){: target='_blank'}.

The New Media Center accepts no liability for copyright infringements. The legal framework mentioned above applies to everyone within the University of Basel. We take it as a given that you have taken note of it and will comply with these copyright rules and regulations.

# Can I edit the video myself?

Of course! If you would like to edit the video yourself, we will be happy to give you the raw video files. Just bring an external hard drive with sufficient storage space with you to the session (approx. 100-200 GB).

# Can I book more than one camera?

Unfortunately no; we shoot with just one camera during the Studio Days sessions.

# Can I book several time slots in a row and record for longer?

No, we can only offer one time slot per video.

# Can you also host my video?

Unfortunately, we cannot host Studio Days videos.

# How much does the Studio Days service cost?

The service is being launched as a pilot. It is currently free for members of the University of Basel.

