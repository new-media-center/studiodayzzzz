from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.shortcuts import render
from django.urls import path
from django.conf.urls import include
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView

urlpatterns = [ path ("oidc/", include('mozilla_django_oidc.urls')), ]

urlpatterns += i18n_patterns (
    path ( "robots.txt"
         , TemplateView.as_view ( template_name="robots.txt"
                                , content_type="text/plain"
                                )
         ),
    path ( ""
         , lambda request: render(request, "pages/home.html")
         , name='welcome'
         ),
    path ( "faq"
         , lambda request: render(request, "pages/faq.html")
         , name='faq'
         ),
    path ( "contact"
         , lambda request: render(request, "pages/contact.html")
         , name='contact'
         ),
    path ( "ablauf"
         , lambda request: render(request, "pages/ablauf.html")
         , name="ablauf"
         ),
    path (settings.ADMIN_URL, admin.site.urls),
    path ("appointment/", include('appointment.urls')),
    path ("user/", include('user.urls')),
    path ("logout/", include('user.urls')),
    path ("pre-login/" 
         , lambda request: render(request, "pages/pre-login.html")
         , name="pre-login"
         ),
    path ('i18n/', include('django.conf.urls.i18n')),
)

# handler400 = TemplateView.as_view ( template_name="400.html"
#                                   , content_type="text/html"
#                                   )
# handler403 = TemplateView.as_view ( template_name="403.html"
#                                   , content_type="text/html"
#                                   )
# handler404 = TemplateView.as_view ( template_name="404.html"
#                                   , content_type="text/html"
#                                   )
# handler500 = TemplateView.as_view ( template_name="500.html"
#                                   , content_type="text/html"
#                                   )

urlpatterns += [
    path("400/", default_views.bad_request),
    path("403/", default_views.permission_denied),
    path("404/", default_views.page_not_found),
    path("500/", default_views.server_error),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path("__debug__/", include(debug_toolbar.urls)),
    ] + urlpatterns
