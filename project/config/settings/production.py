from .base import * # noqa
ALLOWED_HOSTS = [
    # '.unibas.ch',
    'studiodays.unibas.ch',
    'nmc-studiodays.nmc.unibas.ch'
]

SECRET_KEY = os.environ['DJANGO_SECRET']

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "studiodays",
        "USER": "postgres",
        "PASSWORD": os.environ.get("PG_PASSWORD", ""),
        "HOST": "postgres",
        "PORT": "5432",
    },
}


OIDC_OP_AUTHORIZATION_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/authorize'
OIDC_OP_JWKS_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/keyset'
OIDC_OP_TOKEN_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/token'
OIDC_OP_USER_ENDPOINT = 'https://login.eduid.ch/idp/profile/oidc/userinfo'
OIDC_RP_SCOPES = 'openid profile email https://login.eduid.ch/authz/User.Read'
OIDC_RP_CLIENT_ID = 'unibas_oidc_client_19883'
OIDC_RP_CLIENT_SECRET = os.environ['OIDC_CLIENT_SECRET']

USE_X_FORWARDED_HOST = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
