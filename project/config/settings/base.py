import os

AUTHENTICATION_BACKENDS = [
    'user.oidcHelper.MyOIDCAB',
    'django.contrib.auth.backends.ModelBackend'
]


CONFIG_DIR = os.path.dirname(os.path.dirname(__file__))
BASE_DIR = os.path.join(CONFIG_DIR, '../')

SECRET_KEY = 'z17*+9#gpdd#_kssvfw=i*v4^(8#7p#nwgp)thew)$2u9+dv)c'

DEBUG = False

ALLOWED_HOSTS = [
    # '.unibas.ch',
    '*'
]

Q_CLUSTER = {
    'name': 'studiodays',
    'redis': {
        'host': 'redis',
        'port': 6379,
        'db': 0,
        'password': None,
        'socket_timeout': None,
        'charset': 'utf-8',
        'errors': 'strict',
        'unix_socket_path': None
    }
}

# Configure sites framework
# See https://docs.djangoproject.com/en/1.10/ref/settings/#sites
SITE_ID = 1

DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.admin',  # modeltranslation needs to be loaded before admin
)

THIRD_PARTY_APPS = (
    'mozilla_django_oidc',
    'django_q',
)

LOCAL_APPS = ( 'appointment'
             , 'user'
             )

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'config.urls'

ACCOUNT_EMAIL_MAX_LENGTH = 190

EMAIL_HOST = "smtp.unibas.ch"
EMAIL_PORT = 25

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(CONFIG_DIR, '../templates'),
            os.path.join(BASE_DIR, 'locale'),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

LANGUAGES = [ ("en", "Englich")
           , ("de", "Deutsch")
           ] 

TIME_ZONE = 'UTC'

# USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = [
    str(os.path.join(CONFIG_DIR, '../locale')),
]
ADMINS = (
    ("""admin""", 'notifications-nmc@unibas.ch'),
)

# Location of root django.contrib.admin URL, use {% url 'admin:index' %}
ADMIN_URL = "admin/"

STATIC_URL = '/static/'

OIDC_RP_SIGN_ALGO = 'RS256'
OIDC_TOKEN_USE_BASIC_AUTH = True

LOGGING = {
    'version': 1,
    'formatters': {
        'verbose': {
            # 'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'WARNING',
            'propagate': True,
        },
    }
}

LOGIN_REDIRECT_URL = "/appointment/meine-reservierung"
LOGOUT_REDIRECT_URL = "/"
