import os

from .base import *

import logging

PROJECT_DIR = os.path.join(BASE_DIR, '../')

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_DIR, 'new_db.sqlite3'),
    }
}

LOCAL_APPS = (
    'django.contrib.staticfiles',
    'debug_toolbar',
)

INSTALLED_APPS = INSTALLED_APPS + LOCAL_APPS  # noqa: F405

MIDDLEWARE = MIDDLEWARE + (  # noqa: F405
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INTERNAL_IPS = [
    '127.0.0.1',
]

ALLOWED_HOSTS = [
    # '.example.com',  # Allow domain and subdomains
    # '.example.com.',  # Also allow FQDN and subdomains
    '*',
]

OIDC_OP_AUTHORIZATION_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/authorize'
OIDC_OP_JWKS_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/keyset'
OIDC_OP_TOKEN_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/token'
OIDC_OP_USER_ENDPOINT = 'https://login.test.eduid.ch/idp/profile/oidc/userinfo'
OIDC_RP_SCOPES = "openid profile email https://login.eduid.ch/authz/User.Read"
OIDC_RP_CLIENT_SECRET = os.environ['OIDC_CLIENT_SECRET']
OIDC_RP_CLIENT_ID = 'unibas-studiodays-test'

# logger = logging.getLogger('django_auth_ldap')
# logging.basicConfig(filename='example.log', level=logging.DEBUG)
# logger.addHandler(logging.StreamHandler())
# logger.setLevel(logging.DEBUG)

# the collectstatic management command will collect static files into this directory.
STATIC_ROOT = os.path.join(PROJECT_DIR, 'project/static')
STATIC_URL = 'static/'
STATICFILES_DIRS = [ os.path.join(PROJECT_DIR, 'static') ]

MEDIA_ROOT = os.path.join(PROJECT_DIR, "media")
MEDIA_URL = '/media/'
