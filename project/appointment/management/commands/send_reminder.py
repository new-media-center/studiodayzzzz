from appointment.models import Slot
from django.core import mail
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from django.utils.timezone import localdate

from datetime import timedelta
from datetime import datetime

from django_q.tasks import schedule

def remind_them():
        today = localdate()
        tomorrow = today + timedelta(days=2)
        atomorrow = today + timedelta(days=3)
        slots = Slot.objects.filter(reservation__isnull=False).filter(start__gte=tomorrow).filter(start__lte=atomorrow)

        subject = "Studiodays Reminder"

        # email client
        for s in slots:
            usr = s.reservation.user

            message = render_to_string ( 'reminder.jinja'
                                       , { 'first_name': usr.first_name
                                         , 'last_name': usr.last_name
                                         }
                                       ) 

            email = mail.EmailMessage ( subject
                                      , message
                                      , 'contact-nmc@unibas.ch'
                                      , [ usr.email ]
                                      )
            email.send ()

class Command(BaseCommand):
    def handle(self, *args, **options):
        remind_them()

schedule( 'django.core.management.call_command',
          'send_reminder'
        , schedule_type='D'
        , next_run=datetime.utcnow().replace(hour=6, minute=0)
        )
