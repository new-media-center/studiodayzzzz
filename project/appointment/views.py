from django.contrib.auth.models import User
from django.core import mail
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.timezone import localdate
from django.utils.translation import get_language

from babel.dates import format_date, format_datetime
from calendar import monthrange
from datetime import date

from .models import Slot, Reservation
from .forms import ReservationForm, SlotForm
from .calendarHelper import EventCalendar

def meinresView(request, pk=None):

    if not request.user.is_authenticated:
        raise PermissionDenied

    if request.method == "GET" and pk and request.GET.get("delete", None):
        res = Reservation.objects.get(pk=pk)

        lang = get_language()
        subject = "Cancellation of appointment"

        if lang == 'de':
            subject = "Bestätigung Stornierung" 
        else:
            lang = 'en' # make sure language is either 'de' or 'en'

        # email client
        message = render_to_string ( 'cancel_confirmation_{}.jinja'.format(lang)
                                   , { 'first_name': request.user.first_name
                                     , 'last_name': request.user.last_name
                                     }
                                   ) 

        email = mail.EmailMessage ( subject
                                  , message
                                  , 'contact-nmc@unibas.ch'
                                  , [ request.user.email ]
                                  )
        email.send ()

        # email for nmc
        message = '{} {} hat den Slot {} storniert.'.format ( res.user.first_name
                                                            , res.user.last_name
                                                            , res.slot
                                                            )
        email = mail.EmailMessage ( 'Stornierung'
                                  , message
                                  , 'contact-nmc@unibas.ch'
                                  , ['contact-nmc@unibas.ch']
                                  )
        email.send ()
        res.delete()
        return redirect(reverse('meires'))

    elif request.method == "GET":
        today = localdate()
        res = Reservation.objects.filter (user=request.user).filter (slot__start__gte=today)
        v = []
        a = v.append
        for r in res:
            a(render_to_string("meinresLine.html", {"res": r}))
        return render ( request, "meinres.html", {"resTable": v}, )



def resView(request, pk=None):

    if not request.user.is_authenticated:
        raise PermissionDenied

    res = Reservation.objects.get (pk=pk) if pk else Reservation()

    if request.method == "POST":
        form = ReservationForm(request.POST)

        # Staff member wants to confirm a reservation
        if ( not form.is_valid()
             and pk
             and form.data['confirmed'] == 'on'
             and request.user.is_staff
           ):
            res.confirmed = True
            res.save()
            
            lang = get_language()
            subject = "Confirmation of your Studio Days appointment"

            if lang == 'de':
                subject = "Bestätigung Ihres Studio-Days-Termins"
            else:
                lang = 'en' # make sure language is either 'de' or 'en'

            message = render_to_string( 'booking_confirmation_{}.jinja'.format(lang)
                                      , { 'first_name': res.user.first_name
                                        , 'last_name': res.user.last_name
                                        , 'slot': format_datetime(res.slot.start, locale=lang)
                                        }
                                      ) 

            email = mail.EmailMessage( subject
                                     , message
                                     , 'contact-nmc@unibas.ch'
                                     , [ res.user.email ]
                                     )
            email.send()
            return redirect(reverse('slot-list'))

        # new reservation coming in
        elif form.is_valid():
            if not pk:
                res.user = request.user
            res.department = form.cleaned_data["department"]
            res.notes = form.cleaned_data["notes"]
            res.slot = form.cleaned_data["slot"]
            res.save()

            message = render_to_string( 'inform_nmc-team_about_a_new_reservation_de.jinja'
                                      , { 'first_name': request.user.first_name
                                        , 'last_name': request.user.last_name
                                        , 'email': request.user.email
                                        , 'slot': res.slot
                                        }
                                      ) 
            email = mail.EmailMessage( 'Studiodays Reservierung'
                                     , message
                                     , 'contact-nmc@unibas.ch'
                                     , [ 'contact-nmc@unibas.ch' ]
                                     )
            email.send()

            lang = get_language()
            subject = "Confirmation of appointment request"

            if lang == 'de':
                subject = "Bestätigung Terminanfrage" 
            else:
                lang = 'en' # make sure language is either 'de' or 'en'

            message = render_to_string( 'request_confirmation_{}.jinja'.format(lang)
                                      , { 'first_name': request.user.first_name
                                        , 'last_name': request.user.last_name
                                        }
                                      ) 

            email = mail.EmailMessage( subject
                                     , message
                                     , 'contact-nmc@unibas.ch'
                                     , [ request.user.email ]
                                     )
            email.send()

            return redirect(reverse('meires'))

    elif request.method == "GET":
        slotPk = request.GET.get("slot", None)
        slot = None
        form = None
        if slotPk:
            slot = Slot.objects.get(pk=slotPk)
            form = ReservationForm (initial={'slot': slot})
        if pk:
            form = ReservationForm(instance=res)
        else:
            form = ReservationForm(request.GET)
        return render(request, "reservation.html", {"form": form, "slot": slot})

    elif request.method  == "DELETE" and pk:
        res.delete ()

def sloView (request, pk=None):

    if not request.user.is_authenticated:
        raise PermissionDenied

    slot = Slot.objects.get (pk=pk) if pk else Slot ()

    if request.method == "POST":
        form = SlotForm (request.POST)
        if form.is_valid ():
            slot.start = form.cleaned_data["start"]
            slot.end = form.cleaned_data["end"]
            slot.save ()
            return redirect(reverse('slot-list'))

    elif request.method == "GET":
        form = SlotForm (instance=slot) if pk else SlotForm (request.GET)
        return render (request, "slotForm.html", {"form": form})

def calView(request):
    slots = Slot.objects.all()
    yearMonths = sorted (list (set ([ (s.start.year, s.start.month) for s in slots ])))
    cal = EventCalendar()
    monthsHtml = [cal.formatmonth(ym[0],ym[1]) for ym in yearMonths]
    return render ( request
                  , "calendar.html"
                  , { "slots": slots, "cals": monthsHtml }
                  ,
                  )

def adminSlotList(request):
    if not request.user.is_staff:
        raise PermissionDenied
    today = localdate()
    slots = Slot.objects.filter(start__gte=today)
    sList = []
    a = sList.append
    for s in slots:
        a(render_to_string( "adminSlotLine.html"
                          , context = {"date": s, "user": request.user }
                          , request=request
                          ))
    return render ( request, "adminSlots.html", {"htmlSlotList": sList}, )

def adminSlotDelete(request, pk):
    if not request.user.is_staff:
        raise PermissionDenied
    Slot.objects.get(pk=pk).delete()
    return redirect(reverse('slot-list'))

def freeSlots(request):
    today = localdate()
    monthRangeInOneYear = monthrange(today.year+1, today.month)
    overviewEnd  = date(today.year+1, today.month, monthRangeInOneYear[1])
    slots = Slot.objects.filter(start__gte=today).filter(start__lte=overviewEnd).filter(reservation=None)
    sList = []
    a = sList.append
    for s in slots:
        a(render_to_string("bookSlotLine.html", {"date": s, "user": request.user }))
    return render ( request, "freeSlots.html", {"htmlSlotList": sList}, )
