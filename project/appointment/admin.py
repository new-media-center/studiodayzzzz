from django.contrib import admin
from .models import Slot

class SlotAdmin(admin.ModelAdmin):
    list_display = ['user', 'start', 'end', 'notes']

    def save_model(self, request, obj, form, change):
        obj.user = request.user
        super().save_model(request, obj, form, change)

admin.site.register(Slot)
