const dayclick = e => {
      const dialog = e.target.querySelector("dialog")
      dialog.showModal()
}

const adjust_slot_end = e => {
  const endInput = document.querySelector("input#id_end")
  endInput.value = e.target.value
}

// const launchConfirmationDialog = e => {
//   const dialog = document.querySelector("dialog.confirmation")
//   dialog.returnValue = "";
//   dialog.style.setProperty("transform", "translate(0,50%)")
//   dialog.showModal()
//   debugger
//   console.log('gigis')
//   debugger
//   console.log(dialog.returnValue)
//   return false
//   //dialog.returnValue
// }

const burgerclick = e => {
  const navBar = document.querySelector(".navbar ul")
  if (getComputedStyle(navBar).opacity < 1)
  {
    navBar.style.setProperty("transform", "translate(0,3rem)")
    navBar.style.setProperty("opacity", 1)
  } else {
    navBar.style.setProperty("transform", "translate(0,-100%)")
    navBar.style.setProperty("opacity", 0)
  }
}
